<?php
namespace EML\CmsBundle\Services;

class Cart{
    
    var $headers;
    var $items=array();
    var $created;
    var $total_qty=0;
    var $total_price=0;
    var $salesId=NULL;
    
    function __construct(){
        $this->created=time();
        $this->headers=array('order_id'=>time()."_".rand(1,999));
    }
    
    public function getCreated(){
        return $this->created;
    }
    
    public function setSalesId($salesId){
        $this->salesId=$salesId;
        return $this;
    }
    public function getSalesId(){
        return $this->salesId;
    }
    
    
    
    function read(){
        return $this->items;
    }
    
    
    function set_total_price(){
        $total_price='0';
        if($this->items){
            foreach ($this->items AS $k=>$I){
                if(isset($I['total_price']))
                    $total_price = number_format( ($total_price+$I['total_price']),2,".","" );
            }
        }
        $this->total_price = $total_price;
    }
            
    function set_total_qty(){
        $total_qty='0';
        if($this->items){
            foreach ($this->items AS $k=>$I){
                $total_qty=$total_qty+$I['cart_qty'];
            }
        }
        $this->total_qty = $total_qty;
        //return $total_qty;
    }
    
    function add($id,$qty,$itemInfo,$parentItemInfo=null){
        //Check if item exists
        //print_r($this->items[$id]['cart_qty']);
        $unit_price = $itemInfo->getPrice();
                
        if( isset($this->items[$id])){
            //Add qty
            if($this->items[$id]['cart_qty'] + $qty <=0 )
            {
                return $this->removeItem($id);
            }
            else
                $this->items[$id]['cart_qty']=( $this->items[$id]['cart_qty'] + $qty );
        }else{
            //Create Item
            $this->items[$id]['cart_qty']=$qty;
            $this->items[$id]['info']=$itemInfo;
            $this->items[$id]['parent_info']=$parentItemInfo;
            
            $this->items[$id]['unit_price']=$unit_price;
        }
        
        $this->items[$id]['total_price']=  number_format( ($unit_price*$this->items[$id]['cart_qty']),2,".","" );
            
        $this->set_total_qty();
        $this->set_total_price();
        
        return array(
            'id_added' => $id, 
            'actual_qty' => $this->items[$id]['cart_qty'],
            'cart'=>$this->items
                );
    }
    
    
    function removeItem($id)
    {
        if(count($this->items)>1)
        {
            unset($this->items[$id]);
        }else{
            $this->items=array();
        }
    }
    
    
    
    
}