<?php

namespace EML\CmsBundle\Services\Extension;

use \Twig_Filter_Function;
use \Twig_Filter_Method;

class TwigExtension extends \Twig_Extension
{

    protected $doctrine;

    public function __construct($doctrine=NULL,$em=NULL)
    {
        $this->doctrine = $doctrine;
        $this->em = $em;
    }

    /**
     * Return the functions registered as twig extensions
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(

            'print_r' => new \Twig_Function_Function('print_r'),

            'file_exists' => new \Twig_Function_Function('file_exists'),
            'get_element_by_id_category' => new \Twig_Function_Method($this,'get_element_by_id_category'),
            'get_childs_by_id_category'  => new \Twig_Function_Method($this,'get_childs_by_id_category'),

            /* 22 lug 2015 | Jacopo aggiunta funzione */
            'get_categories_by_id_area'  => new \Twig_Function_Method($this,'get_categories_by_id_area'),

            'getExtrafieldBySlug' =>  new \Twig_Function_Method($this,'get_extrafield_by_slug'),


            'getColorPalette' =>  new \Twig_Function_Method($this,'get_color_palette'),
        );
    }




    function get_color_palette($imageFile, $numColors = 5, $granularity = 5)
    {
       $granularity = max(1, abs((int)$granularity));
       $colors = array();

       $size = @getimagesize($imageFile);
       if($size === false)
       {
          user_error("Unable to get image size data for resources: ".$imageFile);
          return false;
       }
       $img = @imagecreatefromstring(file_get_contents($imageFile));

       if(!$img)
       {
          user_error("Unable to open image file");
          return false;
       }
       for($x = 0; $x < $size[0]; $x += $granularity)
       {
          for($y = 0; $y < $size[1]; $y += $granularity)
          {
             $thisColor = imagecolorat($img, $x, $y);
             $rgb = imagecolorsforindex($img, $thisColor);
             $red = round(round(($rgb['red'] / 0x33)) * 0x33);
             $green = round(round(($rgb['green'] / 0x33)) * 0x33);
             $blue = round(round(($rgb['blue'] / 0x33)) * 0x33);
             $thisRGB = sprintf('%02X%02X%02X', $red, $green, $blue);
             if(array_key_exists($thisRGB, $colors))
             {
                $colors[$thisRGB]++;
             }
             else
             {
                $colors[$thisRGB] = 1;
             }
          }
       }
       arsort($colors);
       return array_slice(array_keys($colors), 0, $numColors);
}


    public function get_extrafield_by_slug($extrafields,$slug)
    {
        $findIt = NULL;
        foreach ($extrafields as $I)
        {
            if($I->getSlug()==$slug) { $findIt = $I->getValue();break; }
        }
        return $findIt;
    }


    /*
     * IMPORTANT ! ! !
     * * * * * * * * * * * * * * * * * * * * * * * *
     * To use this functions
     * in the services of the bundle twig extension
     * need to verify doctrine and Entity Manager are passed as argument:
     * Something like this:
     # # # # # # # # # # # # # # # # # # # # # # # #
     # arguments:
     #        doctrine: "@doctrine"
     #        em: "@doctrine.orm.entity_manager"
     # # # # # # # # # # # # # # # # # # # # # # # #
     */
    public function get_element_by_id_category($idCategory,$LOCAL,$idToExclude=NULL,$onlyThisView=NULL,$orderby=array(),$limit=NULL,$limit_from=NULL) //$LOCAL in Twig is app.request.locale OR app.request.getLocale()
    {
        $em=$this->em;
        $qb = $em->createQueryBuilder();
        $dql='
                e.id, e.title, e.slug, e.description, e.keywords, e.h1,
                e.h2, e.text, e.abstract, e.weight, e.listed, e.isaccessible,
                e.redirect, e.lang, e.createdon, e.modifyon, e.price ';
        $qb->select($dql)
            ->from('EMLCmsBundle:Element', 'e')
            ->innerJoin('e.categories','c')
            ->where('e.isaccessible = 1')
            ->andWhere(' e.parent IS NULL ')
            ->andWhere(" e.lang = '".$LOCAL."' ");

        if(!empty($idCategory))
            $qb->andWhere('c.id = :idCategory')->setParameter('idCategory', $idCategory);

        $qb->groupBy('e.id');

        if(empty($orderby) || !is_array($orderby)) {
          $qb->addOrderBy("e.weight","ASC");
        } else {
          $qb->addOrderBy($orderby[0],$orderby[1]);
        }

        if(!empty($idToExclude)){
            //Add $idToExclude where
            $qb->andWhere(" e.id != '".$idToExclude."' ");
        }

        if(!empty($onlyThisView)){
            //Add $idToExclude where
            $qb->andWhere(" e.view = '".$onlyThisView."' ");
        }

        if(!empty($limit_from))
            $qb->setFirstResult($limit_from);
        if(!empty($limit))
            $qb->setMaxResults($limit);

            $elements = $qb->getQuery()
            ->getResult();
        return $elements;
    }

    public function get_childs_by_id_category($idCategory,$LOCAL) //$LOCAL in Twig is app.request.locale OR app.request.getLocale()
    {
        //$em=$this->em;
        $doctrine = $this->doctrine;
        $repo = $doctrine->getRepository('EMLCmsBundle:Category');
        //$categories = $repo->findBy(array('isaccessible'=>'1','idCategory'=>$idCategory,'lang'=>$LOCAL));
        $categories = $repo->findBy(
                array('isaccessible'=>'1','idCategory'=>$idCategory,'lang'=>$LOCAL),
                array('weight' => 'ASC')
                );
        //$doctrine = $this->doctrine;
        return $categories;
    }

    /* 22 lug 2015 | Jacopo aggiunta funzione | Start */
    public function get_categories_by_id_area($idArea,$LOCAL) //$LOCAL in Twig is app.request.locale OR app.request.getLocale()
    {
        $doctrine = $this->doctrine;
        $repo = $doctrine->getRepository('EMLCmsBundle:Category');
        //$categories = $repo->findBy(array('isaccessible'=>'1','idCategory'=>$idCategory,'lang'=>$LOCAL));
        $categories = $repo->findBy(
                array('isaccessible'=>'1','idArea'=>$idArea,'lang'=>$LOCAL),
                array('slug' => 'ASC')
                );
        //$doctrine = $this->doctrine;
        return $categories;
    }
    /* 22 lug 2015 | Jacopo aggiunta funzione
     *               --> bravo! commento by @gabriele on 24 ago 2015
     *                   quando lo trovi fa un fischio ;)
     * | End */


    public function getName()
    {
        return 'twig_extension';
    }
}
