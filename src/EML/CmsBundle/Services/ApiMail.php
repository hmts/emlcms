<?php /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *                                                                           *
 *      /.\                   .|'''', '||`                        ||         *
 *     // \\              ''  ||       ||   ''                    ||         *
 *    //...\\    '||''|,  ||  ||       ||   ||  .|''|, `||''|,  ''||''       *
 *   //     \\    ||  ||  ||  ||       ||   ||  ||..||  ||  ||    ||         *
 * .//       \\.  ||..|' .||. `|....' .||. .||. `|...  .||  ||.   `|..'      *
 *                ||                                                         *
 *               .||                                                         *
 *                                                                           *   
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *  Client for Email Sender API                                              *
 *  Code desigend for HMTS                                                   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *  Author: Gabriele Marazzi                                                 *
 *  ven 8 agosto 2014                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

class ApiClient {
    
    var $API_URL;
    var $PRIVATE_KEY;
    var $public_key;
    
    var $emailTestingDefault="gabriele.marazzi@gmail.com";
    
    private $requestUrl;
    /*
     * Construct the API settigns
     * API_URL, PRIVATE_KEY, public_key
     * * * * * * * * * * * * * * * * * * */
    function init($API_URL,$PRIVATE_KEY,$public_key){
        $this->API_URL = $API_URL;
        $this->PRIVATE_KEY = $PRIVATE_KEY;
        $this->public_key = $public_key;
    }
    
    
    /*
     * Send Post request
     * * * * * * * * * * * * * * * * * * */
    function sendPOST($URL,$postData){ 
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        $enCRYPTING = $this->enCrypt(serialize($postData),$this->PRIVATE_KEY);
        $postData_EnCrypt['request'] = $enCRYPTING;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 
            http_build_query( $postData_EnCrypt ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_USERAGENT, $useragent);
        $result = curl_exec ($ch);
        curl_close ($ch);
        //print_r($result);
        return $result;
    }
    
    /*
     * Transform Json to Array 
     * * * * * * * * * * * * * * * * * * */
    function makeCallArray($ApiCall,$postData){
        $response=$this->makeCall($ApiCall,$postData);
        return json_decode($response); 
    }
    
    /*
     * Send Request
     * * * * * * * * * * * * * * * * * * */
    function makeCall($ApiCall,$postData){
        $this->makeUrl($ApiCall);
        //print_r($this->requestUrl);
        return $this->sendPOST($this->requestUrl,$postData);
    }
    
    /*
     * Prepare Request Uri
     * Accept an array or a string:
     * 1. If Array first paramter is the slug uri
     * append to Url_Api and second are additional 
     * querystring parameters 
     * 2. If String is the slug uris
     * * * * * * * * * * * * * * * * * * */
    function makeUrl($ApiCall){
        $time=time(); $hash=hash_hmac('ripemd160', $time."-".$this->public_key, $this->PRIVATE_KEY);
        if(is_array($ApiCall)){
            $this->requestUrl = $this->API_URL.$ApiCall[0]
                    ."?public_key=" . $this->public_key."&time=".$time."&token=".$hash."&".$ApiCall[1];
        }else{
            $this->requestUrl = $this->API_URL
                    .$ApiCall . "?public_key=".$this->public_key."&time=".$time."&token=".$hash."&";
        }        
    }
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  
     *      .d8888b.    8888888b.    Y88b   d88P   8888888b.    88888888888
     *     d88P  Y88b   888   Y88b    Y88b d88P    888   Y88b       888    
     *     888    888   888    888     Y88o88P     888    888       888    
     *     888          888   d88P      Y888P      888   d88P       888    
     *     888          8888888P"        888       8888888P"        888    
     *     888    888   888 T88b         888       888              888    
     *     Y88b  d88P   888  T88b        888       888              888    
     *      "Y8888P"    888   T88b       888       888              888   
     *   
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */ 
    
    /*
     * Encoding on base64 in a safty way
     * in order to have no different result
     * * * * * * * * * * * * * * * * * * */
    public  function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 
    /*
     * Decoding on base64 in a safty way
     * * * * * * * * * * * * * * * * * * */
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    /*
     * Encrypt data
     * * * * * * * * * * * * * * * * * * */
    public  function enCrypt($value,$secretKey){ 
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $secretKey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }
 
    /*
     * Decrypt data
     * * * * * * * * * * * * * * * * * * */
    public function deCrypt($value,$secretKey){ 
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $secretKey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
}