<?php

namespace EML\CmsBundle\Controller;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EML\CmsBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{

    public function indexAction(Request $request)
    {
        $session = $request->getSession();

        /*
         * Perform redirect for _local
         */
        $params=array(); //home_select_language.html.twig
        if($this->get('templating')->exists('EMLCmsBundle:Home:home_select_language.html.twig') )
            return $this->render('EMLCmsBundle:Home:home_select_language.html.twig', $params);
        else
            return $this->redirect($this->generateUrl('eml_cms_home_lang', $params));
    }

    public function homeAction(Request $request)
    {

        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        if ($this->container->has('Cart'))
            $Cart = $this->get('Cart');

        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);

        $sections = $Globalizer->getSections($LOCAL,NULL);
        //echo '<pre>';print_r($sections);echo '</pre>';
        //echo'<pre>';print_r($featured_home);echo'</pre>';

        /*
        $homeElement = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element')
            ->findOneBy(array('slug'=>'home','lang'=>$LOCAL))
        */

            $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Element');
            $qb = $repository->createQueryBuilder('e');

            $homeElementLoop = $qb
                //->where('e.isaccessible = 1')
                //->andWhere(" e.parent IS NULL ")
                ->where("e.slug = 'home'")
                ->andWhere(" e.lang = '".$LOCAL."' ")
                //->andWhere(" e.h1 LIKE '%".$q."%' ")
                ->getQuery()
                ->getArrayResult();
                if($homeElementLoop && isset($homeElementLoop[0]))
                  $homeElement = $homeElementLoop[0];
                else
                  $homeElement = null;


        $homeElements = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element')
            ->findBy(array('inhome'=>'1','lang'=>$LOCAL));


        $viewParams = array(
            'page'=>$homeElement,
            'elements'=>$homeElements,
            'sections' => $sections,
            'featured' => $featured_home,
            'menu' => $menu
        );

        #$viewParams['Variables']=json_encode($viewParams,true);


        $viewParams['Variables']=$viewParams;
        //echo'<pre>';print_r($viewParams);echo'</pre>';

        $type = "Home";
        $views = 'EMLCmsBundle:Home:index.html.twig';
        return $this->sendResponse("Home",$type,$views,$viewParams,$request);

        //return $this->render('EMLCmsBundle:Home:index.html.twig', $viewParams);

    }

    public function qAction(Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);

        $sections = $Globalizer->getSections($LOCAL,NULL);
        //echo '<pre>';print_r($sections);echo '</pre>';
        //echo'<pre>';print_r($featured_home);echo'</pre>';

        //$qb = $this->getDoctrine()->getManager()->createQueryBuilder('e');
        $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Element');
        $qb = $repository->createQueryBuilder('e');
        $q = $this->get('request')->request->get('q');

        $elements = $qb
            ->where('e.isaccessible = 1')
            ->andWhere(" e.parent IS NULL ")
            ->andWhere(" e.lang = '".$LOCAL."' ")
            //->andWhere(" e.h1 LIKE '%".$q."%' ")
            ->andWhere(
                $qb->expr()->like('e.h1', ':q')
            )
            ->setParameter('q','%'.$q.'%')
            ->getQuery()
            ->getArrayResult();
            //->execute();

        $viewParams = array(
            'q'=>$q,
            'sections' => $sections,
            'name' => 'world',
            'elements'=>$elements,
            'featured' => $featured_home,
            'menu' => $menu
        );

        $type = "q";
        $views = 'EMLCmsBundle:Home:search_results.html.twig';
        return $this->sendResponse("Home",$type,$views,$viewParams,$request);
        //return $this->render('EMLCmsBundle:Home:search_results.html.twig', $viewParams);
    }


    public function sendAction(Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);

        $sections = $Globalizer->getSections($LOCAL,NULL);


        $mail_user = $this->container->getParameter('mail_user');
        $mail_contact_info = $this->container->getParameter('mail_contact_info');

        $postData = $this->get('request')->request->all();

        //$sito = $this->container->get('router')->getContext()->getHost();
        //$sito = $this->get('request')->getHost();
        $sito = $_SERVER['REQUEST_URI'];
        $sito = str_replace("/", "-", $sito);
        $sito = explode("-", $sito);

        $siteName = $sito[1];


        //if($env=='prod'){
        //    $siteName = $sito[0];
        //}

        if($this->container->hasParameter('site_name')){
                $site_name = $this->container->getParameter('site_name');
                $siteName = $site_name;
        }

        $appDomain = "";
        if($this->container->hasParameter('app_domain')){
                $app_domain = $this->container->getParameter('app_domain');
                $appDomain = $app_domain;
        }


        $subj = "Contatti dal sito - ".$siteName;

        /*
          Check CSS HoneyPot if is set for this istance of the CMS
        */
        if($this->container->hasParameter('form_honeypot')){
          $formHoneyPot = $this->container->hasParameter('form_honeypot');

            if(isset($postData["email1"]) && !empty($postData["email1"])){
              /*
                The HoneyPot is full!
                the request has send by a BOT
              */
              return false;
            }
        }

        /*
          Check if form name exists - - -
        */
        $form_name = false;
        if(isset($postData['form_name']) && !empty($postData['form_name'])){
          $form_name = $postData['form_name'];
        }

        /*
          Check if forms is defined - - -
        */
        $forms = false;
        $autoresponder = false;
        if($this->container->hasParameter('forms')){
                $forms = $this->container->getParameter('forms');

                if(isset($forms[$form_name])){
                  /* override $mail_user if to is defined */
                  if(isset($forms[$form_name]['to']) && !empty($forms[$form_name]['to']) )
                    $mail_user = $forms[$form_name]['to'];
                  /* override $mail_contact_info if ccn is defined */
                  if(isset($forms[$form_name]['ccn']) && !empty($forms[$form_name]['ccn']) )
                      $mail_contact_info = $forms[$form_name]['ccn'];

                  /* override $subj if ccn is defined */
                  if(isset($forms[$form_name]['subject']) && !empty($forms[$form_name]['subject']) )
                    $subj = $forms[$form_name]['subject'];

                  /*
                    Check if autoresponder has to be set
                  */
                  if($forms[$form_name]['autoresponder'] && isset($postData['email']) && !empty($postData['email'])){
                    $autoresponder = true;

                  }
                }
        }
        //$mail_user = $this->container->getParameter('mail_user');
        //$mail_contact_info = $this->container->getParameter('mail_contact_info');



        /*
          Check if recaptcha_secret is present
          and if it is check for write value!
          as in the official guide: https://developers.google.com/recaptcha/docs/verify
        */
        $recaptcha_secret = false;
        if($this->container->hasParameter('recaptcha_secret')){
                $recaptcha_secret = $this->container->getParameter('recaptcha_secret');
        }

        /*
          Check recaptcha if present
        */
        if($recaptcha_secret && !empty($postData) && !isset($postData['nore'])){
          //extract data from the post
          //set POST variables
          $url = 'https://www.google.com/recaptcha/api/siteverify';
          $fields = array(
          	'secret' => $recaptcha_secret,
          	'response' => @$postData['g-recaptcha-response'],
          	'remoteip' => $request->getClientIp()
          );
          //url-ify the data for the POST
          $fields_string="";
          foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; } rtrim($fields_string, '&');
          //open connection
          $ch = curl_init();
          //set the url, number of POST vars, POST data
          curl_setopt($ch,CURLOPT_URL, $url);
          curl_setopt($ch,CURLOPT_POST, count($fields));
          curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
          //execute post
          $result = curl_exec($ch);
          $result = json_decode($result);
          if(!$result->success){
            /* Error on reCaptcha send back the user */

            if($this->get('templating')->exists('EMLCmsBundle:Home:send_error.html.twig')){

              $sendElement = $this->getDoctrine()
                  ->getRepository('EMLCmsBundle:Element')
                  ->findOneBy(array('slug'=>'senderror','lang'=>$LOCAL));
              $viewParams = array(
                  'sections' => $sections,
                  'page' => $sendElement,
                  'featured' => $featured_home,
                  'menu' => $menu
              );

              return $this->render('EMLCmsBundle:Home:send_error.html.twig', $viewParams);
            }else{
            echo "Error in the recaptcha field.<br>Please <a href=\"#\" onclick=\"history.go(-1);return false;\">[Go Back]</a>. ";
            if($this->get('kernel')->getEnvironment()=="dev"){
                echo '<div style="padding:9px;background:#ffffcc;border:1px solid #afafaf;margin:5px;">';
                echo 'To customize this message create a EMLCmsBundle:Home:send_error.html.twig ';
                echo '<br>and add a Element with a slug like: "senderror"';
                echo '</div>';
            }
            }
            //$this->redirect($request->server->get('HTTP_REFERER'));
            exit;
          }

          //close connection
          curl_close($ch);
          //
        }

        //
        $emailVars=array(
          'emailVars'=>$postData,
          'appDomain'=>$appDomain,
          'siteName'=>$siteName,
          'featured'=>$featured_home,
          'sections'=>$sections,
          'menu'=>$menu
        );



        //Add timestamp to the subject
        $subj = $subj ." ". time();
        $subj_autoresponder = $subj;
        if(isset($postData['subj'])){
          $subj_autoresponder = $postData['subj'];
        }


        /*
        Send the form to the owner of the site
        */
            $emailContent = \Swift_Message::newInstance()
                ->setSubject($subj)
                ->setFrom($mail_user)
                ->setTo($mail_user)
                ->setBcc($mail_contact_info)
                ->setBody( $this->renderView('EMLCmsBundle:Email:contacts.html.twig',$emailVars),'text/html' );

        $SendEmail = $this->get('mailer')->send($emailContent);

        /*
        Send autoresponder to trhe user
        */
        if($autoresponder){
          $views = $this->setEmailView($LOCAL,$form_name);
          $emailContent = \Swift_Message::newInstance()
              ->setSubject($subj_autoresponder)
              ->setFrom($mail_user)
              ->setTo($postData['email'])
              ->setBody( $this->renderView($views,$emailVars),'text/html' );

          $SendEmail = $this->get('mailer')->send($emailContent);
        }


        $sendElement = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element')
            ->findOneBy(array('slug'=>'send','lang'=>$LOCAL));


        //Set a new welcome page if the record is presnet
        if($form_name){
            $slug_autoresponder = "send-" . $form_name;
            $sendElement_autoresponder = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element')
            ->findOneBy(array('slug'=>$slug_autoresponder,'lang'=>$LOCAL));

            if($sendElement_autoresponder){

                $sendElement = $sendElement_autoresponder;
                $params = array('slug'=>$sendElement_autoresponder->getSlug());
                return $this->redirect($this->generateUrl('eml_cms_element', $params));
            }
        }



        $viewParams = array(
            'sections' => $sections,
            'name' => 'world',
            'page' => $sendElement,
            'featured' => $featured_home,
            'menu' => $menu
        );

        return $this->render('EMLCmsBundle:Home:send_result.html.twig', $viewParams);
    }


    private function setEmailView($LOCAL,$viewName = NULL){
        $viewName=($this->get('templating')->exists('EMLCmsBundle:Autoresponder:' . $viewName . "-" . $LOCAL . '.html.twig')
                && !empty($viewName))
                ?$viewName:"default-" . $LOCAL;
        return 'EMLCmsBundle:Autoresponder:'.$viewName . "-" . $LOCAL . '.html.twig';
    }



    public function sitemapindexAction(Request $request)
    {
      $session = $request->getSession();
      $languagesString = $this->container->getParameter('eml_cms_bundle.languages');
      //print_r($languagesString);
      $languages = explode("|",$languagesString);
      //print_r($languages);
      $urls = array();

      foreach ($languages as $langParam) {
        $urls[] = array(
            'loc' => $this->get('router')->generate('eml_cms_home_lang', array('_locale'=>$langParam) ),
            'lastmod' => date('c')
        );
      }

      //print_r($urls); print_r("--------");

      $rootNode = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

      //$baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
      $baseurl = $request->getScheme() . '://' . $request->getHttpHost() ;

      foreach ($urls AS $url)
      {

          $loc = $baseurl.$url['loc'];

          $itemNode = $rootNode->addChild('url');
          $itemNode->addChild( 'loc', $loc );
          if(isset($url['lastmod']))
              $itemNode->addChild( 'lastmod', $url['lastmod'] );

      }


      $response = new Response( $rootNode->asXML() );
      $response->headers->set('Content-Type', 'text/xml');
      //$response->send();
      return $response;

    }
    public function sitemapAction(Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $em = $this->getDoctrine()->getEntityManager();

        $urls = array();
        $hostname = $this->getRequest()->getHost();

        // add some urls homepage
        $urls[] = array(
            'loc' => $this->get('router')->generate('eml_cms_home_lang'),
            'changefreq' => 'weekly', 'priority' => '1.0'
        );

        // multi-lang pages
        /*
        foreach($languages as $lang) {
            $urls[] = array('loc' => $this->get('router')->generate('home_contact', array('_locale' => $LOCAL)), 'changefreq' => 'monthly', 'priority' => '0.3');
        }
        */

        // urls from database
        //$urls[] = array('loc' => $this->get('router')->generate('home_product_overview', array('_locale' => $LOCAL)), 'changefreq' => 'weekly', 'priority' => '0.7');
        // service

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Area');
        //$ele = $repo->findOneBySlug($slug);
        $Area = $repo->findBy(array('lang'=>$LOCAL,'isaccessible'=>'1'));

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Category');
        //$ele = $repo->findOneBySlug($slug);
        $Category = $repo->findBy(array('lang'=>$LOCAL,'isaccessible'=>'1'));

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element');
        //$ele = $repo->findOneBySlug($slug);
        $Elements = $repo->findBy(array('lang'=>$LOCAL,'isaccessible'=>'1','parent'=>null));



        foreach ($Area as $item) {
            $urls[] = array('loc' => $this->get('router')->generate('eml_cms_area',
                    array('idArea' => $item->getId(), 'slug' => $item->getSlug())), 'priority' => '0.5');
        }

        foreach ($Category as $item) {
            $urls[] = array(
                'loc' => $this->get('router')->generate('eml_cms_cat', array('slug' => $item->getSlug())),
                'priority' => '0.7',
                'changefreq' => 'weekly'
            );
        }

        foreach ($Elements as $item) {
            $urls[] = array('loc' => $this->get('router')->generate('eml_cms_element',
                    array('slug' => $item->getSlug())), 'priority' => '0.5');
        }

        //return array('urls' => $urls, 'hostname' => $hostname);



        $rootNode = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

        //$baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() ;

        foreach ($urls AS $url)
        {

            $loc = $baseurl.$url['loc'];

            $itemNode = $rootNode->addChild('url');
            $itemNode->addChild( 'loc', $loc );
            if(isset($url['lastmod']))
                $itemNode->addChild( 'lastmod', $url['lastmod'] );
            if(isset($url['changefreq']))
                $itemNode->addChild( 'changefreq', $url['changefreq'] );
            if(isset($url['priority']))
                $itemNode->addChild( 'priority', $url['priority'] );


        }
        // ...


        $response = new Response( $rootNode->asXML() );
        $response->headers->set('Content-Type', 'text/xml');
        //$response->send();
        return $response;

    }


    /*
      sendResponse()
    */
    private function sendResponse($name,$type,$views,$viewParams,$request){
      //header('Access-Control-Allow-Origin: *');
      $headers = $request->headers->all();

      $jsonParams = $viewParams;
      $jsonGet = $request->query->get('json');
      $getSections = $request->query->get('getSections');

      if( isset($headers['json']) || !empty( $jsonGet ) ){

        //print_r($headers);

        $response = new Response();
        $jsonParams['name'] = $name;
        $jsonParams['type'] = $type;
        $jsonParams['views'] = $views;

        /*
          Avoid to send section data if not required
        */
        if(empty($getSections)){
          unset($jsonParams['sections']);
        }

        if(isset($jsonParams['elements'])){
            //$jsonParams['elements'] =  (array) $jsonParams['elements'];
        }

        /*
          Check MD5
        */
        $getMd5 = $request->query->get('md5');
        if(!empty($getMd5)){
          $jsonParams = md5(serialize(json_encode( $jsonParams )));
        }
        $response->setContent(json_encode( $jsonParams ));
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');

        return $response;

      }else{
        return $this->render($views, $viewParams);
      }
    } //sendResponse()
}
