<?php

namespace EML\CmsBundle\Controller;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EML\CmsBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class CartController extends Controller
{

    public function indexAction(Request $request)
    {   
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();
        
        if ($this->container->has('Cart'))
            $Cart = $this->get('Cart');
        
        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $sections = $Globalizer->getSections($LOCAL,NULL);
        
        $cartElement = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element')
            ->findOneBy(array('slug'=>'home','lang'=>$LOCAL));
        
        $viewParams = array(
            'page'=>$cartElement,
            'sections' => $sections,
            'featured' => $featured_home,
            'menu' => $menu
        );
        $CartItems=NULL;
        $Cart=$session->get( '_CART_' );
        //print_r($Cart);
        if( !empty($Cart) ){
            $CartItems = $Cart->read();
            //print_r($CartItems);
        }
        
        $viewParams['Cart'] = $CartItems;
        
        $viewParams['Variables']=$viewParams;
        //echo'<pre>';print_r($viewParams);echo'</pre>';
        return $this->render('EMLCmsBundle:Cart:index.html.twig', $viewParams);
    }

    
    public function addAction(Request $request)
    {   
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();
        
        if ($this->container->has('Cart'))
            $Cart = $this->get('Cart');
        
        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $sections = $Globalizer->getSections($LOCAL,NULL);
        
        //Create the Cart Object 
        $Cart=$session->get( '_CART_' );
        if( empty($Cart) ){
            $session->set( '_CART_', $this->get('Cart') );
            $Cart=$session->get( '_CART_' );
        }
        
        $stock_id = $request->query->get('id');
        $qty = $request->query->get('qty');
        
        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element');
        $Product = $repo->findOneBy(array('id'=>$stock_id,'isaccessible'=>'1'));
        
        if (empty($Product) || !$Product)
            throw new NotFoundHttpException("Page not found");
        
        //$ParentProduct=empty($Product->getParent())?NULL:$Product->getParent();
        $ParentProduct=NULL;
        $idParent = $Product->getParent()->getId();
        if(!empty($idParent) && $idParent!=0)
            $ParentProduct=$repo->findOneBy(array('id'=>$idParent));
                    
                        
        $addResponse = $Cart->add( $stock_id,$qty,$Product,$ParentProduct );
        //print_r($addResponse);
        
        $isAjax = $request->query->get('ajax');
        if(isset($isAjax))
            return new JsonResponse($addResponse);
        
        return $this->redirect($this->getRequest()->headers->get('referer'));
    }
    
    
    public function sendAction(Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();
        
        if ($this->container->has('Cart'))
            $Cart = $this->get('Cart');
        
        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        
        $sections = $Globalizer->getSections($LOCAL,NULL);
        
        $mail_user = $this->container->getParameter('mail_user');
        $postData = $this->get('request')->request->all();
        
        $CartItems = NULL;
        $Cart = $session->get( '_CART_' );
        if( !empty($Cart) ){
            $CartItems = $Cart->read();
        }
        
        $viewParams['Cart'] = $CartItems;
        
        
        $emailVars=array('emailVars'=>$postData);
                
        
        //Save the cart and empty the session _CART_
        if( !empty($Cart) )
        {
            $session->set( '_SAVE_CART_', $session->get( '_CART_' ) );
        }
        $session->remove('_CART_');
        
        $emailVars['Cart'] = $session->get( '_SAVE_CART_' );
        
        $subj = "Nuovo Ordine";
            $emailContent = \Swift_Message::newInstance()
                ->setSubject($subj)
                ->setFrom($mail_user) 
                ->setTo($mail_user)
                ->setBody( $this->renderView('EMLCmsBundle:Cart:Email/default.html.twig',$emailVars),'text/html' );
            
        $SendEmail = $this->get('mailer')->send($emailContent);
        
        //print_r($session->get( '_SAVE_CART_' ));
        $viewParams = array(
            'Cart' => $session->get( '_SAVE_CART_' ),
            'sections' => $sections,
            'name' => 'world',
            'featured' => $featured_home,
            'menu' => $menu
        );
        
        return $this->render('EMLCmsBundle:Cart:send_result.html.twig', $viewParams);
    }
    
    
    public function clearAction(Request $request)
    {   
        $LOCAL = $this->get('request')->getLocale();
        
        $session = $request->getSession();
        $session->remove('_CART_');
        $addResponse=array('cleared'=>true);
        
        $isAjax = $request->query->get('ajax');
        if(isset($isAjax))
            return new JsonResponse(array('cleared'=>true));
        
        $location=$this->getRequest()->headers->get('referer');
            if(empty($location)){
                $location = $this->generateUrl('eml_cms_cart');
            }
        return $this->redirect( $location );        
    }
    
}
