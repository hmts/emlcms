<?php

namespace EML\CmsBundle\Controller;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EML\CmsBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\HttpFoundation\Response;


class PageController extends Controller
{
    public function elementAction(Request $request,$slug)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();



        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Element');
        //$ele = $repo->findOneBySlug($slug);
        $ele = $repo->findOneBy(array('lang'=>$LOCAL,'slug'=>$slug,'isaccessible'=>'1','parent'=>null));
        //->andWhere(" e.parent IS NULL ")

        if($ele)
        {
            $idElement = $ele->getId();

            $eleCat = $ele->getCategories();
            $eleChilds = $repo->findBy(
                    array('parent'=>$idElement,'isaccessible'=>'1'),
                    array('weight'=>'ASC')
                    );

            $extraFields = $ele->getExtrafields();
            $extraFieldsArr=array();
            if($extraFields)
                foreach ($extraFields AS $I)
                    $extraFieldsArr[$I->getSlug()]=$I->getValue();
            //print_r($extraFieldsArr);

            //print_r($eleChilds);
        }


        if (empty($ele) || !$ele)
            throw new NotFoundHttpException("Page not found");


        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Category');

        $eleCatArea=null;
        $catParentcat=null;
        $catChildcat=null;
        if(isset($eleCat[0]))
        {
            $firstCat = $repo->findOneById($eleCat[0]->getId());
            $eleCatArea = $firstCat->getArea();
            $catParentcat = $firstCat->getParentcat();
            $catChildcat = $firstCat->getChildscat();
        }
        //echo'<pre>';print_r($eleCat);echo'</pre>';



        $Globalizer = $this->get('Globalizer');
        $sections = $Globalizer->getSections($LOCAL,NULL);
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $featured = $Globalizer->getFeatured($LOCAL,NULL,$idElement);
        $image = $Globalizer->getImages($LOCAL,NULL,$idElement);
        $attach = $Globalizer->getAttachments($LOCAL,NULL,$idElement);
        $link = $Globalizer->getLinks($LOCAL,NULL,$idElement);
        $tags = $Globalizer->getTags($LOCAL,NULL,NULL);

        $viewParams = array(
            'name' => 'world',
            'sections' => $sections,
            'slug' => $slug,
            'featByEle' => $featured,
            'featured' => $featured_home,
            'element' => $ele,
            'extrafields'=>$extraFieldsArr,

            'eleChilds'=>$eleChilds,
            'eleFirstCat' =>$eleCat[0],
            'catParentcat' => $catParentcat,
            'catChildcat' => $catChildcat,
            'eleAllCat' =>$eleCat,
            'eleCatArea'=>$eleCatArea,
            'menu' => $menu,
            'image' => $image,
            'attach' => $attach,
            'link' => $link,
            'tags' => $tags,
        );

        $type=$ele->getView();
        $views = $this->setView("Element",$type);


        return $this->sendResponse("Element",$type,$views,$viewParams,$request);

        //return $this->render($views, $viewParams);
    }

    public function areaAction($idArea, $slug, Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Area');

        //$area = $repo->findOneBySlug($slug);
        $area = $repo->findOneBy(array('slug'=>$slug,'isaccessible'=>'1'));

        if (empty($area) || ($area->getId() != $idArea))
            throw new NotFoundHttpException("Page not found");
        //$category = $Sectioner->get($idArea);

        /*
        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Category');
        $cat = $repo->findByIdArea($idArea);
        */



        $Globalizer = $this->get('Globalizer');
        $sections = $Globalizer->getSections($LOCAL,NULL);
        $categories = $Globalizer->getCategories($LOCAL,$idArea);
        //echo'<pre>';print_r($categories);echo'</pre>';exit;
        //echo'<pre>';print_r($sections);echo'</pre>';exit;
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $tags = $Globalizer->getTags($LOCAL,NULL,NULL);

        $viewParams = array(
            'name' => 'world',
            'sections'=>$sections,
            'slug' => $slug,
            'area' => $area,
            //'cat' => $cat,
            'categories' => $categories,
            'featured' => $featured_home,
            //'r2' => $r2,
            'menu' => $menu,
            'tags' => $tags,

        );
        $type=$area->getView();
        $views = $this->setView("Area",$type);
        return $this->sendResponse("Area",$type,$views,$viewParams,$request);
        //return $this->render($views, $viewParams);
    }


    public function areaTagAction($idArea, $slug, $tagslug, Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Area');

        //$area = $repo->findOneBySlug($slug);
        $area = $repo->findOneBy(array('slug'=>$slug,'isaccessible'=>'1'));

        if (empty($area) || ($area->getId() != $idArea))
            throw new NotFoundHttpException("Page not found");

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Tags');
        $tag = $repo->findOneBySlug($tagslug);
        if (empty($tag) || ($tag->getSlug() != $tagslug) ||
                (
                    $tag->getIdArea() != $idArea
                )
           )
            throw new NotFoundHttpException("Page not found");

        //$category = $Sectioner->get($idArea);

        /*
        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Category');
        $cat = $repo->findByIdArea($idArea);
        */



        $Globalizer = $this->get('Globalizer');
        $sections = $Globalizer->getSections($LOCAL,NULL);
        $categories = $Globalizer->getCategories($LOCAL,$idArea);
        //echo'<pre>';print_r($categories);echo'</pre>';exit;
        //echo'<pre>';print_r($sections);echo'</pre>';exit;
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $tags = $Globalizer->getTags($LOCAL,NULL,NULL);

        $viewParams = array(
            'sections'=>$sections,
            'slug' => $slug,
            'area' => $area,
            'tag' => $tag,
            //'cat' => $cat,
            'categories' => $categories,
            'featured' => $featured_home,
            //'r2' => $r2,
            'menu' => $menu,
            'tags' => $tags,
        );
        $type=$area->getView();
        $views = $this->setView("AreaTag",$type);
        return $this->sendResponse("AreaTag",$type,$views,$viewParams,$request);
        //return $this->render($views, $viewParams);
    }

    public function categoryAction($slug, $page, Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        if($page=="1")
            return $this->redirect($this->generateUrl('eml_cms_cat', array('slug'=>$slug)));
        if($page==NULL)
            $page="1";




        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Category');

        //$cat = $repo->findOneBySlug($slug);
        $cat = $repo->findOneBy(array('slug'=>$slug,'isaccessible'=>'1','lang'=>$LOCAL));

        /*
          Add for get a JSON array
        */
        $qb = $repo->createQueryBuilder('c');
        $catLoop = $qb
            ->where('c.isaccessible = 1')
            ->andWhere("c.slug = '".$slug."'")
            ->andWhere(" c.lang = '".$LOCAL."' ")

            ->getQuery()
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getArrayResult();




        if (empty($cat))
            throw new NotFoundHttpException("Page not found");


        $idCategory = $cat->getId();


        $catArea = $cat->getArea();
        $catParentcat = $cat->getParentcat();
        $catChildscat = $cat->getChildscat();

        //print_r($catArea);
        if(empty($catArea)){
            if(!empty($catParentcat))
                $catArea = $catParentcat->getArea();
        }

        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $sections = $Globalizer->getSections($LOCAL,NULL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $featured = $Globalizer->getFeatured($LOCAL,$idCategory,NULL);
        $image = $Globalizer->getImages($LOCAL,$idCategory,NULL);
        $attach = $Globalizer->getAttachments($LOCAL,$idCategory,NULL);
        $link = $Globalizer->getLinks($LOCAL,$idCategory,NULL);
        $tags = $Globalizer->getTags($LOCAL,NULL,NULL);

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb2 = $this->getDoctrine()->getManager()->createQueryBuilder();


        //$limit = 2;
        $limit = ($this->container->hasParameter('category_page_limit'))
                ?$this->container->getParameter('category_page_limit')
                :1;

        $from  = (($page * $limit) - $limit);

        $total_count = $qb2->select("COUNT(e)")
            ->from('EMLCmsBundle:Element', 'e')
            ->innerJoin('e.categories','c')
            ->where('e.isaccessible = 1')
            ->andWhere(" e.parent IS NULL ")
            ->andWhere(" e.lang = '".$LOCAL."' ")
            ->andWhere('c.id = :idCategory')->setParameter('idCategory',$idCategory)
            ->getQuery()->getSingleScalarResult();
            //print_r($total_count);
        $total_pages = ceil($total_count / $limit);

        # get child elements:
        $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Element');

        $qb = $repository->createQueryBuilder('e');

        $headers = $request->headers->all();
        $jsonGet = $request->query->get('json');
        if( isset($headers['json']) || !empty( $jsonGet ) ){
          $elements = $qb ->innerJoin('e.categories','c')
          ->where('e.isaccessible = 1')
          ->andWhere(" e.parent IS NULL ")
          ->andWhere(" e.lang = '".$LOCAL."' ")
          ->andWhere('c.id = :idCategory')->setParameter('idCategory',$idCategory)
          ->addOrderBy("e.weight","ASC")
          ->getQuery()
          ->setFirstResult($from)
          ->setMaxResults($limit)
          //->getResult();
          ->getArrayResult();

        }else{
          $elements = $qb ->innerJoin('e.categories','c')
          ->where('e.isaccessible = 1')
          ->andWhere(" e.parent IS NULL ")
          ->andWhere(" e.lang = '".$LOCAL."' ")
          ->andWhere('c.id = :idCategory')->setParameter('idCategory',$idCategory)
          ->addOrderBy("e.weight","ASC")
          ->getQuery()
          ->setFirstResult($from)
          ->setMaxResults($limit)
          ->getResult();
          //->getArrayResult();

        }




            //print_r($elements); echo '<br>@@@@@@<br>'; print_r(json_encode($elements));

        # get child cats:
        $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Category');
        $qb2 = $repository->createQueryBuilder('cc');

            $r2 = $qb2->where('cc.isaccessible = 1')
            ->andWhere('cc.idCategory = :idCategory')->setParameter('idCategory',$idCategory)
            ->getQuery()
            ->execute()
            ;



        /*
         * SetUp the params array for passing
         * all the required vars to the paginators
         * for construct the url
         */
        $routeParams=array('slug'=>$slug);
        $pagination=$Globalizer->pagination(array(
            'from'=>$from,
            'limit'=>$limit,
            'total_pages'=>$total_pages,
            'current_page'=>$page,
            'container'=>$this,
            'routeParams'=>$routeParams
        ));
        if($page>$total_pages && $total_count>0)
            throw new NotFoundHttpException("Page not found");


        $viewParams = array(
            'sections'=>$sections,
            'slug' => $slug,
            'featByCat' => $featured,
            'featured' => $featured_home,
            'cat'=>$cat,
            'category'=>$catLoop[0], //Add for get a JSON array

            'catArea'=>$catArea,
            'catParentcat'=>$catParentcat,
            'catChildscat'=>$catChildscat,
            'elements' => $elements,
            'r2' => $r2,
            'menu' => $menu,
            'image' => $image,
            'attach' => $attach,
            'link' => $link,
            'pagination'=>$pagination,
            'tags' => $tags,
        );




        #$viewParams['Variables']=json_encode($viewParams);
        //print_r($viewParams['Variables']);

        $type=$cat->getView();
        $views = $this->setView("Category",$type);
        return $this->sendResponse("Category",$type,$views,$viewParams,$request);
        //return $this->render($views, $viewParams);
    }


    public function categoryTagAction($slug, $tagslug, Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Category');

        //$cat = $repo->findOneBySlug($slug);
        $cat = $repo->findOneBy(array('slug'=>$slug,'isaccessible'=>'1'));

        if (empty($cat))
            throw new NotFoundHttpException("Page not found");

        $idCategory = $cat->getId();


        $catArea = $cat->getArea();
        $catParentcat = $cat->getParentcat();
        //print_r($catArea);
        if(empty($catArea)){
            if(!empty($catParentcat))
                $catArea = $catParentcat->getArea();
        }


        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Tags');
        $tag = $repo->findOneBySlug($tagslug);

        if (empty($tag) || ($tag->getSlug() != $tagslug))
            throw new NotFoundHttpException("Page not found");
        else if( $tag->getIdArea() != $catArea->getId() && $tag->getIdcategory()!=$cat->getId() && $tag->getIdcategory()!=$cat->getParentcat()->getId()  )
            throw new NotFoundHttpException("Page not found");

        $idTag = $tag->getId();


        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $sections = $Globalizer->getSections($LOCAL,NULL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $featured = $Globalizer->getFeatured($LOCAL,$idCategory,NULL);
        $image = $Globalizer->getImages($LOCAL,$idCategory,NULL);
        $attach = $Globalizer->getAttachments($LOCAL,$idCategory,NULL);
        $link = $Globalizer->getLinks($LOCAL,$idCategory,NULL);
        $tags = $Globalizer->getTags($LOCAL,NULL,NULL);

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

        # get child elements:
        $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Element');
        $qb = $repository->createQueryBuilder('e');

        $elements = $qb->innerJoin('e.categories','c')
            ->innerJoin('e.tags','t')
            ->where('e.isaccessible = 1')
            ->andWhere(" e.parent IS NULL ")
            ->andWhere(" e.lang = '".$LOCAL."' ")
            ->andWhere('c.id = :idCategory')->setParameter('idCategory',$idCategory)
            ->andWhere('t.id = :idTag')->setParameter('idTag',$idTag)
            ->getQuery()
            ->execute();

        # get child cats:
        $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Category');
        $qb2 = $repository->createQueryBuilder('cc');

        $r2 = $qb2->where('cc.isaccessible = 1')
            ->andWhere('cc.idCategory = :idCategory')->setParameter('idCategory',$idCategory)
            ->getQuery()
            ->execute();


        $viewParams = array(
            'sections'=>$sections,
            'slug' => $slug,
            'featByCat' => $featured,
            'featured' => $featured_home,
            'cat'=>$cat,
            'tag'=>$tag,
            'catArea'=>$catArea,
            'catParentcat'=>$catParentcat,
            'elements' => $elements,
            'r2' => $r2,
            'menu' => $menu,
            'image' => $image,
            'attach' => $attach,
            'link' => $link,
            'tags' => $tags,
        );

        #$viewParams['Variables']=json_encode($viewParams);
        //print_r($viewParams['Variables']);

        $type=$cat->getView();
        $views = $this->setView("CategoryTag",$type);
        return $this->sendResponse("CategoryTag",$type,$views,$viewParams,$request);
        //return $this->render($views, $viewParams);
    }

    public function catCategoryAction($slug1, $slug2, Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Category');
        //$cat1 = $repo->findOneBySlug($slug1);
        $cat1 = $repo->findOneBy(array('slug'=>$slug1,'isaccessible'=>'1'));


        if (empty($cat1))
            throw new NotFoundHttpException("Page not found");
        //$cat2 = $repo->findOneBySlug($slug2);
        $cat2 = $repo->findOneBy(array('slug'=>$slug2,'isaccessible'=>'1'));


        if (empty($cat2))
            throw new NotFoundHttpException("Page not found");


        $idCat1 = $cat1->getId();
        $idCat2 = $cat2->getId();

        $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Element');
        $qb = $repository->createQueryBuilder('e');

        $r = $qb->innerJoin('e.categories','c')
            ->where('e.isaccessible = 1')
            ->andWhere(" e.parent IS NULL ")
            ->andWhere(" e.lang = '".$LOCAL."' ")
            ->andWhere('c.id = :idCategory')->setParameter('idCategory',$idCat1)
            ->andWhere('c.id = :idCategory')->setParameter('idCategory',$idCat2)
            ->getQuery()
            ->execute();


        $Globalizer = $this->get('Globalizer');
        $menu = $Globalizer->getMenus($LOCAL);
        $sections = $Globalizer->getSections($LOCAL,NULL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $tags = $Globalizer->getTags($LOCAL,NULL,NULL);

        $viewParams = array(
            'name' => 'world',
            'sections' => $sections,
            'slug1' => $slug1,
            'slug2' => $slug2,
            'cat1' => $cat1,
            'cat2' => $cat2,
            'r' => $r,
            'featured' => $featured_home,
            //'featured' => $featured,
            'menu' => $menu,
            'tags' => $tags,
        );
        $views = 'EMLCmsBundle:Page:cat_cat.html.twig';
        return $this->sendResponse("catCategoryAction",$type,$views,$viewParams,$request);
        //return $this->render('EMLCmsBundle:Page:cat_cat.html.twig', $viewParams);
    }

    public function tagAction($slug, Request $request)
    {
        $LOCAL = $this->get('request')->getLocale();
        $session = $request->getSession();

        $repo = $this->getDoctrine()
            ->getRepository('EMLCmsBundle:Tags');
        $tag = $repo->findOneBySlug($slug);
        $idTag = $tag->getId();

        $repository = $this->getDoctrine()->getRepository('EMLCmsBundle:Element');
        $qb = $repository->createQueryBuilder('e');

        $r = $qb->innerJoin('e.tags','t')
            ->where('e.isaccessible = 1')
            ->andWhere(" e.lang = '".$LOCAL."' ")
            ->andWhere('t.id = :idTag')->setParameter('idTag',$idTag)
            ->getQuery()
            ->execute();

        //$featured = $Featurer->get(NULL, NULL);
        $Globalizer = $this->get('Globalizer');
        $sections = $Globalizer->getSections($LOCAL,NULL);
        $menu = $Globalizer->getMenus($LOCAL);
        $featured_home  = $Globalizer->getFeatured($LOCAL,NULL,NULL);
        $tags = $Globalizer->getTags($LOCAL,NULL,NULL);

        $viewParams = array(
            'name' => 'world',
            'sections'=>$sections,
            'slug' => $slug,
            'featured' => $featured_home,
            //'category' => $category[0],
            'menu' => $menu,
            'tag' => $tag,
            'tags' => $tags,
            'r'=> $r
        );


        $views = 'EMLCmsBundle:Page:tag.html.twig';
        return $this->sendResponse("tagAction",$type,$views,$viewParams,$request);
        //return $this->render('EMLCmsBundle:Page:tag.html.twig', $viewParams);
    }



    private function setView($viewDir,$type=NULL){
        $viewName=($this->get('templating')->exists('EMLCmsBundle:'.$viewDir.':'.$type.'.html.twig')
                && !empty($type))
                ?$type:"default";
        return 'EMLCmsBundle:'.$viewDir.':'.$viewName.'.html.twig';
    }


    /*
      sendResponse()
    */
    private function sendResponse($name,$type,$views,$viewParams,$request){
      //header('Access-Control-Allow-Origin: *');
      $headers = $request->headers->all();

      $jsonParams = $viewParams;
      $jsonGet = $request->query->get('json');
      $getSections = $request->query->get('getSections');

      if( isset($headers['json']) || !empty( $jsonGet ) ){

        //print_r($headers);

        $response = new Response();
        $jsonParams['name'] = $name;
        $jsonParams['type'] = $type;
        $jsonParams['views'] = $views;

        /*
          Avoid to send section data if not required
        */
        if(empty($getSections)){
          unset($jsonParams['sections']);
        }

        if(isset($jsonParams['elements'])){
            //$jsonParams['elements'] =  (array) $jsonParams['elements'];
        }

        /*
          Check MD5
        */
        $getMd5 = $request->query->get('md5');
        if(!empty($getMd5)){
          $jsonParams = md5(serialize(json_encode( $jsonParams )));
        }
        $response->setContent(json_encode( $jsonParams ));
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');

        return $response;

      }else{
        return $this->render($views, $viewParams);
      }
    } //sendResponse()
}
